#pragma once
#include <cstdint>
#include <type_traits>
#include <utility>

namespace io {
        namespace detail {
                template<typename T, typename>
                struct is_reader : std::false_type { };

                template<typename T>
                struct is_reader<
                        T,
                        decltype(std::declval<T>().read((char*)0, (char*)0))
                > : std::true_type { };

                template<typename T, typename>
                struct is_writer : std::false_type { };

                template<typename T>
                struct is_writer<
                        T,
                        decltype(std::declval<T>().write((char const*)0, (char const*)0))
                > : std::true_type { };

                template<typename T, typename, typename, typename>
                struct is_seeker : std::false_type { };

                template<typename T>
                struct is_seeker<
                        T,
                        decltype(std::declval<T>().seek_begin((std::intmax_t)0)),
                        decltype(std::declval<T>().seek_end((std::intmax_t)0)),
                        decltype(std::declval<T>().seek_current((std::intmax_t)0))
                > : std::true_type { };

                template<typename T, typename>
                struct is_fd : std::false_type { };

                template<typename T>
                struct is_fd<
                        T,
                        std::enable_if_t<
                                std::is_same<
                                        typename T::native_handle_type,
                                        decltype(std::declval<T const>().native_handle())
                                >::value
                        >
                > : std::true_type { };
        }

        template<typename T>
        using is_reader = detail::is_reader<T, char*>;

        template<typename T>
        using is_writer = detail::is_writer<T, char const*>;

        template<typename T>
        using is_seeker = detail::is_seeker<T, void, void, void>;

        template<typename T>
        using is_fd = detail::is_fd<T, void>;
}
