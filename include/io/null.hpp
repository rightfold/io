#pragma once
#include <cstdint>
#include "traits.hpp"

namespace io {
        class null_t {
        public:
                char* read(char* begin, char*) {
                        return begin;
                }

                char const* write(char const*, char const* end) {
                        return end;
                }

                void seek_begin(std::intmax_t) { }

                void seek_end(std::intmax_t) { }

                void seek_current(std::intmax_t) { }
        };

        static null_t null;

        static_assert(is_reader<null_t>(), "null must be a reader");
        static_assert(is_writer<null_t>(), "null must be a writer");
        static_assert(is_seeker<null_t>(), "null must be a seeker");
}
