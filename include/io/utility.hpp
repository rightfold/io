#pragma once
#include <boost/filesystem.hpp>
#include "detail/windows.hpp"
#include <set>
#include <system_error>

namespace io {
        class early_eof : public std::runtime_error {
        public:
                using std::runtime_error::runtime_error;
        };

        template<typename Reader>
        void read_full(Reader&& reader, char* begin, char* end) {
                while (begin != end) {
                        auto new_begin = reader.read(begin, end);
                        if (new_begin == begin) {
                                throw early_eof(__func__);
                        }
                        begin = new_begin;
                }
        }

        enum class file_access {
                read,
                write,
                append,
        };

        template<typename Fd>
        Fd create(boost::filesystem::path const& path, std::set<file_access> access, int create_mode) {
                if (access.count(file_access::append)) {
                        access.insert(file_access::write);
                }

                DWORD winapi_access = 0;
                if (access.count(file_access::read)) {
                        winapi_access |= GENERIC_READ;
                }
                if (access.count(file_access::write)) {
                        winapi_access |= GENERIC_WRITE;
                }

                DWORD creation_disposition = 0;
                if (create_mode == -1) {
                        creation_disposition |= OPEN_EXISTING;
                } else {
                        creation_disposition |= CREATE_NEW;
                }
                if (access.count(file_access::write) && !access.count(file_access::append)) {
                        creation_disposition |= TRUNCATE_EXISTING;
                }

                auto handle = CreateFileW(
                        path.c_str(),
                        winapi_access,
                        0,
                        nullptr,
                        creation_disposition,
                        0,
                        nullptr
                );
                if (handle == nullptr) {
                        throw std::system_error(GetLastError(), std::system_category());
                }
                return Fd(handle);
        }

        template<typename Fd>
        Fd open(boost::filesystem::path const& path, std::set<file_access> access) {
                return create<Fd>(path, access, -1);
        }
}
