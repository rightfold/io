#pragma once
#include <cstddef>
#include <cstdint>
#include "detail/windows.hpp"
#include "exception.hpp"
#include <utility>
#include <system_error>

namespace io {
        enum class capability {
                read,
                write,
                seek,
        };

        namespace detail {
                template<typename Self, capability Capability>
                struct implementation_for_capability;

                template<typename Self>
                struct implementation_for_capability<Self, capability::read> {
                        char* read(char* begin, char* end) {
                                auto handle = static_cast<Self*>(this)->fd.native_handle();
                                auto n_to_read = end - begin;
                                DWORD n_read;
                                auto success = ReadFile(handle, begin, n_to_read, &n_read, nullptr);
                                if (!success) {
                                        auto error = GetLastError();
                                        if (error == ERROR_HANDLE_EOF) {
                                                return begin;
                                        } else {
                                                throw std::system_error(error, std::system_category());
                                        }
                                }
                                return begin + n_read;
                        }
                };

                template<typename Self>
                struct implementation_for_capability<Self, capability::write> {
                        char const* write(char const* begin, char const* end) {
                                auto handle = static_cast<Self*>(this)->fd.native_handle();
                                auto n_to_write = end - begin;
                                DWORD n_written;
                                auto success = WriteFile(handle, begin, n_to_write, &n_written, nullptr);
                                if (!success) {
                                        throw std::system_error(GetLastError(), std::system_category());
                                }
                                return begin + n_written;
                        }
                };

                template<typename Self>
                struct implementation_for_capability<Self, capability::seek> {
                        void seek_begin(std::intmax_t offset) {
                                seek(offset, FILE_BEGIN);
                        }

                        void seek_end(std::intmax_t offset) {
                                seek(offset, FILE_END);
                        }

                        void seek_current(std::intmax_t offset) {
                                seek(offset, FILE_CURRENT);
                        }

                private:
                        void seek(std::intmax_t offset, DWORD whence) {
                                auto handle = static_cast<Self*>(this)->fd.native_handle();
                                auto status = SetFilePointer(handle, offset, nullptr, whence);
                                if (status == INVALID_SET_FILE_POINTER) {
                                        throw bad_seek(__func__);
                                }
                        }
                };
        }

        template<typename Fd, capability... Capabilities>
        class file : public detail::implementation_for_capability<file<Fd, Capabilities...>, Capabilities>... {
        public:
                explicit file(Fd fd)
                        : fd(std::move(fd)) { }

        private:
                Fd fd;

                friend class detail::implementation_for_capability<file, capability::read>;
                friend class detail::implementation_for_capability<file, capability::write>;
                friend class detail::implementation_for_capability<file, capability::seek>;
        };
}
