#pragma once
#include <cassert>
#include "detail/windows.hpp"
#include <memory>
#include "traits.hpp"

namespace io {
        class unique_fd {
        public:
                using native_handle_type = HANDLE;

                explicit unique_fd(native_handle_type handle) noexcept
                        : handle(handle) { }

                unique_fd(unique_fd&& other) noexcept
                        : handle(other.handle) {
                        other.handle = nullptr;
                }

                unique_fd& operator=(unique_fd&& other) {
                        close();
                        handle = other.handle;
                        other.handle = nullptr;
                        return *this;
                }

                ~unique_fd() {
                        close();
                }

                native_handle_type native_handle() const noexcept {
                        assert(handle != nullptr);
                        return handle;
                }

        private:
                void close() {
                        if (handle != nullptr) {
                                // TODO: Handle error.
                                CloseHandle(handle);
                        }
                }

                native_handle_type handle;
        };

        static_assert(is_fd<unique_fd>(), "unique_fds must be fds");

        class shared_fd {
        public:
                using native_handle_type = HANDLE;

                explicit shared_fd(native_handle_type handle)
                        : fd(std::make_shared<unique_fd>(handle)) { }

                native_handle_type native_handle() const noexcept {
                        assert(fd != nullptr);
                        return fd->native_handle();
                }

        private:
                std::shared_ptr<unique_fd> fd;
        };

        static_assert(is_fd<shared_fd>(), "shared_fds must be fds");
}
