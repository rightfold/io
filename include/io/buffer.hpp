#pragma once
#include <algorithm>
#include <cstdint>
#include "exception.hpp"
#include "traits.hpp"
#include <vector>

namespace io {
        template<typename Storage = std::vector<char>>
        class buffer {
        public:
                buffer() = default;

                explicit buffer(Storage storage)
                        : storage(std::move(storage))
                        , offset(0) { }

                char* read(char* begin, char* end) {
                        auto n = std::min(
                                std::size_t(end - begin),
                                std::size_t(storage.size() - offset)
                        );
                        begin = std::copy_n(storage.begin() + offset, n, begin);
                        offset += n;
                        return begin;
                }

                void seek_begin(std::intmax_t seek_offset) {
                        if (seek_offset >= static_cast<std::intmax_t>(storage.size())) {
                                throw bad_seek(__func__);
                        }
                        offset = seek_offset;
                }

                void seek_end(std::intmax_t seek_offset) {
                        seek_begin(storage.size() + seek_offset);
                }

                void seek_current(std::intmax_t seek_offset) {
                        seek_begin(offset + seek_offset);
                }

        private:
                Storage storage;
                std::size_t offset;
        };

        static_assert(is_reader<buffer<>>(), "buffers must be readers");
        static_assert(is_seeker<buffer<>>(), "buffers must be seekers");
}
