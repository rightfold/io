#pragma once
#include <stdexcept>

namespace io {
        class bad_seek : public std::runtime_error {
        public:
                using std::runtime_error::runtime_error;
        };
}
