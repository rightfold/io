#pragma once
#include <cassert>
#include <iterator>

namespace io {
        template<typename Reader>
        class reader_iterator : public std::iterator<std::input_iterator_tag, char const> {
        public:
                reader_iterator()
                        : reader(nullptr) { }

                explicit reader_iterator(Reader& reader)
                        : reader(&reader) {
                        read();
                }

                char const& operator*() const noexcept {
                        assert(reader != nullptr);
                        return c;
                }

                char const* operator->() const noexcept {
                        assert(reader != nullptr);
                        return &c;
                }

                reader_iterator& operator++() {
                        assert(reader != nullptr);
                        read();
                        return *this;
                }

                reader_iterator& operator++(int) {
                        return operator++();
                }

        private:
                void read() {
                        assert(reader != nullptr);
                        auto it = reader->read(&c, &c + 1);
                        if (it == &c) {
                                reader = nullptr;
                        }
                }

                char c;
                Reader* reader;

                friend bool operator==(reader_iterator const& a, reader_iterator const& b) {
                        return a.reader == b.reader;
                }

                friend bool operator!=(reader_iterator const& a, reader_iterator const& b) {
                        return !(a == b);
                }
        };
}
