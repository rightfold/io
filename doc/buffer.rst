``buffer`` class template
=========================

In-memory readable, writable and seekable buffer.
