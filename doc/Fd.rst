``Fd`` concept
==============

File descriptor.

Requirements
------------

The type ``T`` satisfies ``Fd`` if:

- ``T`` satisfies ``MoveConstructible``.
- ``T`` satisfies ``MoveAssignable``.
- ``T::native_handle_type`` is a type.
- ``std::declval<T>().native_handle()`` is of type ``T::native_handle_type``.
