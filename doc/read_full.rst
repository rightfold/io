``read_full`` function template
===============================

Either completely fill a buffer, or throw an exception.
