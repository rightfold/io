# io
I/O stream library for C++.

## Principles

 - API is synchronous. If you need async I/O under the hood, use Boost.Coroutine.
 - Concerns are separated, e.g. buffering isn't baked into everything.
 - I/O streams deal with bytes, not text.
 - Interoperate with the standard I/O library using composition; don't force it upon everything.

## Dependencies

This library is header-only and has the following dependencies:

 - Boost.Filesystem
 - Boost.System
 - C++14 standard library
