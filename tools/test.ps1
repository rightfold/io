If (Test-Path build.ninja) {
        rm build.ninja
}

Add-Content build.ninja "builddir = build"

Add-Content build.ninja "cxx = g++"
Add-Content build.ninja "cxxincludes = -IC:\Users\elyse\Catch\include -Iinclude"
Add-Content build.ninja "cxxflags = -std=c++14 -Wall -Wextra -Werror -Wpedantic `$cxxincludes"

Add-Content build.ninja "ld = g++"
Add-Content build.ninja "ldlibraries = -lboost_system -lboost_filesystem"
Add-Content build.ninja "ldflags = `$ldlibraries"

Add-Content build.ninja "rule cxx"
Add-Content build.ninja "  depfile = `$out.d"
Add-Content build.ninja "  command = `$cxx -MMD -MF `$out.d `$cxxflags -c `$in -o `$out"

Add-Content build.ninja "rule ld"
Add-Content build.ninja "  command = `$ld `$in -o `$out `$ldflags"

$sources = gci test\*.cpp | Resolve-Path -Relative
$objects = @()
Foreach ($source in $sources) {
        $object = "`$builddir\$source.o"
        Add-Content build.ninja "build $object`: cxx $source"
        $objects += $object
}
Add-Content build.ninja "build `$builddir\test.exe: ld $objects"

ninja
If ($?) { build\test }
