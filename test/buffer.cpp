#include <catch.hpp>
#include <io/buffer.hpp>
#include "read.hpp"
#include "seek.hpp"
#include <string>
#include <vector>

TEST_CASE("read should read from the buffer", "[buffer]") {
        std::string data("hello\r\nworld\r\n");
        io::buffer<> buf(std::vector<char>(data.begin(), data.end()));
        io::test::test_read(buf);
}

TEST_CASE("seek should seek the buffer", "[buffer]") {
        std::string data("hello\r\nworld\r\n");
        io::buffer<> buf(std::vector<char>(data.begin(), data.end()));
        io::test::test_seek(buf);
}
