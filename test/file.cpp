#include <catch.hpp>
#include <io/fd.hpp>
#include <io/file.hpp>
#include <io/utility.hpp>
#include "read.hpp"
#include "seek.hpp"
#include <utility>

TEST_CASE("read should read from the file", "[file]") {
        auto fd = io::open<io::unique_fd>("test/data/hello.txt", {io::file_access::read});
        io::file<io::unique_fd, io::capability::read> file(std::move(fd));
        io::test::test_read(file);
}

TEST_CASE("seek should seek the file", "[file]") {
        auto fd = io::open<io::unique_fd>("test/data/hello.txt", {io::file_access::read});
        io::file<io::unique_fd, io::capability::read, io::capability::seek> file(std::move(fd));
        io::test::test_seek(file);
}
