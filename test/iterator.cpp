#include <algorithm>
#include <catch.hpp>
#include <io/buffer.hpp>
#include <io/iterator.hpp>
#include <io/null.hpp>
#include <iterator>
#include <string>

TEST_CASE("reader_iterator should handle EOF", "[iterator]") {
        REQUIRE(io::reader_iterator<io::null_t>() == io::reader_iterator<io::null_t>(io::null));
}

TEST_CASE("reader_iterator should work with std::copy", "[iterator]") {
        std::string input("hello\r\nworld\r\n");
        std::string output;
        io::buffer<> buf(std::vector<char>(input.begin(), input.end()));
        std::copy(
                io::reader_iterator<decltype(buf)>(buf),
                io::reader_iterator<decltype(buf)>(),
                std::back_inserter(output)
        );
        REQUIRE(input == output);
}
