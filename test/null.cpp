#include <catch.hpp>
#include <io/null.hpp>
#include <vector>

TEST_CASE("read should return EOF immediately", "[null]") {
        std::vector<char> buffer(10);
        auto it = io::null.read(buffer.data(), buffer.data() + buffer.size());
        REQUIRE(it == buffer.data());
}

TEST_CASE("write should indicate that everything was written", "[null]") {
        std::vector<char> buffer(10);
        auto it = io::null.write(buffer.data(), buffer.data() + buffer.size());
        REQUIRE(it == buffer.data() + buffer.size());
}

TEST_CASE("seek should succeed", "[null]") {
        io::null.seek_begin(0);
        io::null.seek_end(0);
        io::null.seek_current(0);
}
