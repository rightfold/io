#include <catch.hpp>
#include <io/null.hpp>
#include <io/utility.hpp>

TEST_CASE("read_full should succeed for empty buffers", "[utility]") {
        std::vector<char> buf;
        read_full(io::null, buf.data(), buf.data() + buf.size());
}

TEST_CASE("read_full should fail when EOF occurs early", "[utility]") {
        std::vector<char> buf(1);
        REQUIRE_THROWS_AS(
                read_full(io::null, buf.data(), buf.data() + buf.size()),
                io::early_eof
        );
}
