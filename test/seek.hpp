#pragma once
#include <catch.hpp>
#include <string>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

namespace io {
namespace test {
        template<typename Device>
        void test_seek(Device& device) {
                {
                        std::string buffer(7, '\0');
                        device.seek_begin(7);
                        auto end = device.read(&buffer[0], &buffer[0] + buffer.size());
                        REQUIRE(buffer == "world\r\n");
                        REQUIRE(end == &buffer[0] + buffer.size());
                }

                {
                        std::string buffer(4, '\0');
                        device.seek_end(-4);
                        auto end = device.read(&buffer[0], &buffer[0] + buffer.size());
                        REQUIRE(buffer == "ld\r\n");
                        REQUIRE(end == &buffer[0] + buffer.size());
                }

                {
                        std::string buffer(9, '\0');
                        device.seek_begin(2);
                        device.seek_current(3);
                        auto end = device.read(&buffer[0], &buffer[0] + buffer.size());
                        REQUIRE(buffer == "\r\nworld\r\n");
                        REQUIRE(end == &buffer[0] + buffer.size());
                }
        }
}
}

#pragma GCC diagnostic pop
