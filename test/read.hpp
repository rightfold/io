#pragma once
#include <catch.hpp>
#include <string>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

namespace io {
namespace test {
        template<typename Device>
        void test_read(Device& device) {
                {
                        std::string buffer(7, '\0');
                        auto end = device.read(&buffer[0], &buffer[0] + buffer.size());
                        REQUIRE(buffer == "hello\r\n");
                        REQUIRE(end == &buffer[0] + buffer.size());
                }

                {
                        std::string buffer(7, '\0');
                        auto end = device.read(&buffer[0], &buffer[0] + buffer.size());
                        REQUIRE(buffer == "world\r\n");
                        REQUIRE(end == &buffer[0] + buffer.size());
                }

                {
                        std::string buffer(14, '\0');
                        auto end = device.read(&buffer[0], &buffer[0] + buffer.size());
                        REQUIRE(buffer == std::string(14, '\0'));
                        REQUIRE(end == &buffer[0]);
                }
        }
}
}

#pragma GCC diagnostic pop
